import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

const Home = () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
const Login = () => import(/* webpackChunkName: "about" */ '@/views/Login.vue')

// tabView 页面
//authorityManagement
const NetworkingUnit = () => import(/* webpackChunkName: "about" */ '@/views/AuthorityManagement/NetworkingUnit.vue')
const RoleManagement = () => import(/* webpackChunkName: "about" */ '@/views/AuthorityManagement/RoleManagement.vue')
const UserManagement = () => import(/* webpackChunkName: "about" */ '@/views/AuthorityManagement/UserManagement.vue')
//dataMonitoring
const DataMonitoringChild = () => import(/* webpackChunkName: "about" */ '@/views/DataMonitoring/DataMonitoringChild.vue')
const MapMonitoring = () => import(/* webpackChunkName: "about" */ '@/views/DataMonitoring/MapMonitoring.vue')
//statisticalAnalysis
const RealTimeAlarmStatistics = () => import(/* webpackChunkName: "about" */ '@/views/StatisticalAnalysis/RealTimeAlarmStatistics.vue')
const TimeIntervalAlarmStatistics = () => import(/* webpackChunkName: "about" */ '@/views/StatisticalAnalysis/TimeIntervalAlarmStatistics.vue')
const StatisticalReport = () => import(/* webpackChunkName: "about" */ '@/views/StatisticalAnalysis/StatisticalReport.vue')
// largeScreenDisplay
const LargeScreenDisplay = () => import(/* webpackChunkName: "about" */ '@/views/LargeScreenDisplay.vue')
// informationService
const DeviceHistoryData = () => import(/* webpackChunkName: "about" */ '@/views/InformationService/DeviceHistoryData.vue')
const DeviceCommunicationLog = () => import(/* webpackChunkName: "about" */ '@/views/InformationService/DeviceCommunicationLog.vue')
// fireInformation
const FireKnowledgeManagement = () => import(/* webpackChunkName: "about" */ '@/views/FireInformation/FireKnowledgeManagement.vue')
const PublicInformationManagement = () => import(/* webpackChunkName: "about" */ '@/views/FireInformation/PublicInformationManagement.vue')
// informationManagement
const EquipmentInformationManagement = () => import(/* webpackChunkName: "about" */ '@/views/InformationManagement/EquipmentInformationManagement.vue')
const RemoteConfigurationOfSmokeDetector = () => import(/* webpackChunkName: "about" */ '@/views/InformationManagement/RemoteConfigurationOfSmokeDetector.vue')
const RemoteConfigurationOfPowerConsumption = () => import(/* webpackChunkName: "about" */ '@/views/InformationManagement/RemoteConfigurationOfPowerConsumption.vue')
//systemManagement
const GeneralDictionary = () => import(/* webpackChunkName: "about" */ '@/views/SystemManagement/GeneralDictionary.vue')
const SystemLog = () => import(/* webpackChunkName: "about" */ '@/views/SystemManagement/SystemLog.vue')
const SystemFunction = () => import(/* webpackChunkName: "about" */ '@/views/SystemManagement/SystemFunction.vue')

const Index = () => import(/* webpackChunkName: "about" */ '@/views/Index.vue')
const UserInfo = () => import(/* webpackChunkName: "about" */ '@/views/UserInfo.vue')


Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: () => '/Index'
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
    path: '/Index',
    name: 'Index',
    components: {
      default: Index,
      index: Index,
      userInfo: UserInfo,
      dataMonitoringChild: DataMonitoringChild,
      mapMonitoring: MapMonitoring,
      realTimeAlarmStatistics: RealTimeAlarmStatistics,
      timeIntervalAlarmStatistics: TimeIntervalAlarmStatistics,
      statisticalReport: StatisticalReport,
      largeScreenDisplay: LargeScreenDisplay,
      deviceHistoryData: DeviceHistoryData,
      deviceCommunicationLog: DeviceCommunicationLog,
      fireKnowledgeManagement: FireKnowledgeManagement,
      publicInformationManagement: PublicInformationManagement,
      equipmentInformationManagement: EquipmentInformationManagement,
      remoteConfigurationOfSmokeDetector: RemoteConfigurationOfSmokeDetector,
      remoteConfigurationOfPowerConsumption: RemoteConfigurationOfPowerConsumption,
      networkingUnit: NetworkingUnit,
      roleManagement: RoleManagement,
      userManagement: UserManagement,
      generalDictionary: GeneralDictionary,
      systemLog: SystemLog,
      systemFunction: SystemFunction
    }
  },
  {
    path: '/largeScreenDisplay',
    name: 'largeScreenDisplay',
    component: LargeScreenDisplay
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
