export function getEColumns() {
  return [{
    prop: "addressCode",
    label: "设备ID",
    headerAlign: "center",
    align: "left",
    width: "180px",
  },
  {
    prop: "productType",
    label: "产品类型",
    headerAlign: "center",
    align: "left",
  },
  {
    prop: "connType",
    label: "通讯类型",
    headerAlign: "center",
    align: "left",
    width: "120px"
  },
  {
    prop: "iot",
    label: "物联网卡",
    headerAlign: "center",
    align: "left",
    width: "180px",
  },
  {
    prop: "iotStatus",
    label: "网卡状态",
    headerAlign: "center",
    align: "center",
    width: "120px",
    type: 'tag'
  },
  {
    prop: "expireDate",
    label: "到期时间",
    headerAlign: "center",
    align: "left",
    width: "160px",
  },
  {
    prop: "sim",
    label: "SIM卡号",
    headerAlign: "center",
    align: "left",
    width: "120px"
  },
  {
    prop: "gatewayCode",
    label: "网关地址",
    headerAlign: "center",
    align: "right",
    width: "120px"
  },
  {
    prop: "installTime",
    label: "安装时间",
    headerAlign: "center",
    align: "left",
    width: "180px",
  },
  {
    prop: "installFloor",
    label: "安装楼层",
    headerAlign: "center",
    align: "left",
  },
  {
    prop: "installAddress",
    label: "安装地址",
    headerAlign: "center",
    align: "left",
    width: "200px",
  }]
}