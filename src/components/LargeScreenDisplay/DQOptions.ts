export default function setOption(nval: number, uval: number, wval: number, bval: number): any {
  return {
    grid: {
      show: true,
      x: '10',//左
      y: '10',//上
      x2: '10',//右
      y2: '50',//下
      containLabel: true,
      borderWidth: 0,//这样所有的坐标轴都没有了
    },

    tooltip: {
      show: false
    },
    legend: {
      show: false,
      data: ['正常', '离线', '报警', '故障']
    },
    toolbox: {
      show: false
    },
    calculable: true,
    series: [
      {
        name: '设备状态分析',
        type: 'pie',
        radius: ['50%', '70%'],
        roseType: 'radius',
        avoidLabelOverlap: false,
        label: {
          normal: {
            show: false,
            formatter: '{b} ({d}%)',
            position: 'center'
          },
          emphasis: {
            show: false,
            textStyle: {
              fontSize: '12',
              fontWeight: 'bold'
            }
          }
        },
        itemStyle: {
          color: function (params: any) {
            var colorList = ['rgb(141,144,145)', 'rgb(28,196,62)', 'rgb(230,162,60)', 'rgb(230,0,18)'];
            return colorList[params.dataIndex]
          },
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        data: [
          { value: uval, name: '离线', },
          { value: nval, name: '正常', },
          { value: wval, name: '报警', },
          { value: bval, name: '故障' },
        ]
      },
      {
        name: '',
        type: 'pie',
        radius: ['43%', '50%'],
        avoidLabelOverlap: false,
        label: { show: false },
        itemStyle: {
          color: function (params: any) {
            var colorList = ['rgba(141,144,145,0.8)', 'rgba(28,196,62,0.8)', 'rgb(230,162,60, 0.8)', 'rgba(230,0,18,0.8)'];
            return colorList[params.dataIndex]
          },
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        data: [
          { value: uval, name: '离线', },
          { value: nval, name: '正常', },
          { value: wval, name: '报警', },
          { value: bval, name: '故障' },
        ]

      }
    ]
  }
}