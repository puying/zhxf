import echarts from "echarts";
export default function setOption(callback: Function) {
  let mapName = 'china'
  let data: any = [];
  let geoCoordMap: any = {};
  let toolTipData: any = [];
  let eobj: any = echarts.getMap(mapName).geoJson
  let mapFeatures: any = eobj.features;
  mapFeatures.forEach(function (v: any) {
    // 地区名称
    var name = v.properties.name;
    // 地区经纬度
    geoCoordMap[name] = v.properties.cp;
    data.push({
      name: name,
      value: Math.round(Math.random() * 100 + 10)
    });
    toolTipData.push({
      name: name,
      value: []
    });
  });
  let max = 480,
    min = 9; // todo
  let maxSize4Pin = 50,
    minSize4Pin = 20;

  let convertData = function (data: any) {
    let res = [];
    for (var i = 0; i < data.length; i++) {
      var geoCoord = geoCoordMap[data[i].name];
      if (geoCoord) {
        res.push({
          name: data[i].name,
          value: geoCoord.concat(data[i].value),
        });
      }
    }
    return res;
  };
  return {
    tooltip: {
      trigger: 'item',
      triggerOn: 'click',
      formatter: function (params: any) {
        var toolTiphtml = '';
        if (typeof (params.value)[2] === "undefined") {
          for (var i = 0; i < toolTipData.length; i++) {
            if (params.name === toolTipData[i].name) {
              toolTiphtml += toolTipData[i].name;
              for (var j = 0; j < toolTipData[i].value.length; j++) {
                toolTiphtml += toolTipData[i].value[j].name + ':' + toolTipData[i].value[j].value + "<br>";
              }
            }
          }
        } else {
          for (var a = 0; a < toolTipData.length; a++) {
            if (params.name === toolTipData[a].name) {
              toolTiphtml += toolTipData[a].name;
              for (var b = 0; b < toolTipData[a].value.length; b++) {
                toolTiphtml += toolTipData[a].value[b].name + ':' + toolTipData[a].value[b].value + "<br>";
              }
            }
          }
        }
        //console.log(toolTiphtml);
        let Pro = toolTiphtml;
        // PageData.GetDataJson();

        // LoadChart('echarts_warn_1', "烟感");
        // LoadChart('echarts_warn_2', "电气火灾");
        // GetWDGrid();
        if (callback) {
          callback(Pro)
        }
        return toolTiphtml;
      }
    },
    legend: {
      orient: 'vertical',
      y: 'bottom',
      x: 'right',
      data: ['credit_pm2.5'],
      textStyle: {
        color: '#fff'
      }
    },
    visualMap: {
      show: false,
      min: 0,
      max: 500,
      left: 'left',
      top: 'bottom',
      text: ['高', '低'], // 文本，默认为数值文本
      calculable: true,
      seriesIndex: [1],
      inRange: {
        color: ['rgb(5,55,80)'] // 蓝绿

      }
    },
    /*工具按钮组*/
    toolbox: {
      show: false,
      orient: 'vertical',
      left: 'right',
      top: 'center',
      feature: {

        dataView: {
          readOnly: false
        },
        restore: {},
        saveAsImage: {}
      }
    },
    geo: {
      show: true,
      map: mapName,
      zoom: 0.8,
      label: {
        normal: {
          show: false
        },
        emphasis: {
          show: false
        }
      },
      roam: true,
      itemStyle: {
        normal: {
          areaColor: '#031525',
          borderColor: 'rgb(2,158,206)',
          borderWidth: 2,

        },
        emphasis: {
          areaColor: '#2B91B7'
        }
      }
    },
    series: [{
      name: '散点',
      type: 'scatter',
      coordinateSystem: 'geo',
      data: convertData(data),
      symbolSize: function (val: any) {
        return val[2] / 10;
      },

      label: {
        normal: {
          formatter: '{b}',
          position: 'right',
          show: false
        },
        emphasis: {
          show: false
        }
      },
      itemStyle: {
        normal: {
          color: '#fff'
        }
      }
    },
    {
      type: 'map',
      map: mapName,
      geoIndex: 0,
      aspectScale: 0.75, //长宽比
      showLegendSymbol: false, // 存在legend时显示
      label: {
        normal: {
          show: true
        },
        emphasis: {
          show: false,
          textStyle: {
            color: '#fff'
          }
        }
      },
      roam: true,
      itemStyle: {
        normal: {
          areaColor: '#031525',
          borderColor: '#3B5077',
        },
        emphasis: {
          areaColor: '#2B91B7'
        }
      },
      animation: false,
      data: data
    },
    {
      name: '点',
      type: 'scatter',
      coordinateSystem: 'geo',
      symbol: 'pin', //气泡
      symbolSize: function (val: any) {
        var a = (maxSize4Pin - minSize4Pin) / (max - min);
        var b = minSize4Pin - a * min;
        b = maxSize4Pin - a * max;
        return a * val[2] + b;
      },
      label: {

        normal: {
          show: false,
          formatter: function (params: any) { return params.data.value[2] },
          textStyle: {
            color: '#fff',
            fontSize: 9,
          }
        }
      },
      itemStyle: {

        normal: {
          color: 'rgba(255,255,255,0)', //标志颜色
        }
      },
      zlevel: 6,
      data: convertData(data),
    },
    {
      name: 'Top 5',
      type: 'effectScatter',
      coordinateSystem: 'geo',
      data: convertData(data.sort(function (a: any, b: any) {
        return b.value - a.value;
      }).slice(0, 5)),
      symbolSize: function (val: any) {
        return val[2] / 10;
      },
      showEffectOn: 'render',
      rippleEffect: {
        brushType: 'stroke'
      },
      hoverAnimation: true,
      label: {
        normal: {
          formatter: '{b}',
          position: 'right',
          show: true
        }
      },
      itemStyle: {
        normal: {
          color: 'rgba(255,255,255,0.8)',
          shadowBlur: 10,
          shadowColor: '#fff'
        }
      },
      zlevel: 1
    }

    ]
  }
}