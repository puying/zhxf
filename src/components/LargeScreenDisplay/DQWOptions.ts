export default function setOption(lval: number, olval: number, otval: number, elval: number): any {
  return {

    tooltip: {
      show: false
    },
    legend: {
      show: false,
      data: ['漏电', '过载', '超温', '其它']
    },
    toolbox: {
      show: false
    },
    calculable: true,
    series: [
      {
        name: '报警状态分析',
        type: 'pie',
        radius: ['50%', '70%'],
        roseType: 'radius',
        avoidLabelOverlap: false,
        label: {
          normal: {
            show: false,
            formatter: '{b} ({d}%)',
            position: 'center'
          },
          emphasis: {
            show: false,
            textStyle: {
              fontSize: '18',
              fontWeight: 'bold'
            }
          }
        },
        itemStyle: {
          color: function (params: any) {
            let WColorList = ['#EE9A00', '#1C86EE', '#ff4500', '#838B8B']
            return WColorList[params.dataIndex];
          }
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        data: [
          { value: lval, name: '漏电' },
          { value: olval, name: '过载' },
          { value: otval, name: '超温' },
          { value: elval, name: '其它' }
        ]
      },
      {
        name: '',
        type: 'pie',
        radius: ['43%', '50%'],
        avoidLabelOverlap: false,
        label: {
          normal: {
            show: false,
            formatter: '{b} ({d}%)',
            position: 'center'
          },
          emphasis: {
            show: false,
            textStyle: {
              fontSize: '18',
              fontWeight: 'bold'
            }
          }
        },
        itemStyle: {
          color: function (params: any) {
            var colorList = ['rgba(255,169,0,0.8)', 'rgba(30,147,255,0.8)', 'rgba(255,69,0,0.8)', 'rgba(131,139,139,0.8)'];
            return colorList[params.dataIndex]
          },
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        data: [
          { value: lval, name: '漏电' },
          { value: olval, name: '过载' },
          { value: otval, name: '超温' },
          { value: elval, name: '其它' }
        ]
      }
    ]
  }
}
