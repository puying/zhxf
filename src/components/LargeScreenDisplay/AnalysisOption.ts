import echarts from "echarts";
export default function setOption(data: any): any {
  return {
    grid: {
      x: '50',//左
      y: '50',//上
      x2: '44',//右
      y2: '26',//下
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {            // 坐标轴指示器，坐标轴触发有效
        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
      },
      formatter: function (params: any) {
        var tar = params[0];
        return tar.name + '<br/>' + tar.seriesName + ' : ' + tar.value;
      }
    },
    xAxis: {
      type: 'category',
      data: data.timeList,
      axisLine: {
        lineStyle: {
          type: 'solid',
          color: 'rgba(255,255,255,0.1)',
          width: 1,   //这里是坐标轴的宽度,这样显示x轴，而不显示x轴对面的轴
        },
        onZero: true
      },
      axisLabel: {
        show: true,
        textStyle: {
          color: '#fff'
        }
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: ['rgba(255,255,255,0.1)'],
          width: 1,
          type: 'solid'
        }
      },
    },
    yAxis: {
      name: '次数',
      type: 'value',
      axisLine: {
        lineStyle: {
          type: 'solid',
          color: 'rgba(255,255,255,0.1)',
          width: 1,   //这里是坐标轴的宽度,这样显示y轴，而不显示y轴对面的轴
        },
        onZero: true
      },
      axisLabel: {
        formatter: '{value}',
        textStyle: {
          color: '#fff'
        }
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: ['rgba(255,255,255,0.1)'],
          width: 1,
          type: 'solid'
        }
      },
    },
    series: [{
      data: data.warnList,
      type: 'line',
      name: '报警次数',
      stack: '总量',
      showAllSymbol: true,
      smooth: true,
      itemStyle: {
        normal: {
          color: '#C5F200',
          areaStyle: {
            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0, color: 'rgba(255,255,255,0.5)' // 0% 处的颜色
            }, {
              offset: 0.4, color: 'rgba(31,119,129,0.9)' // 100% 处的颜色
            }, {
              offset: 1, color: 'rgba(0,0,0,0.5)' // 100% 处的颜色
            }]
            ),
          },
          lineStyle: { color: 'rgb(94,214,215)' }
        }
      },
    }]
  }
}