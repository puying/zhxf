export default function setOption(loraval: number, nbval: number): any {
  return {
    color: ['rgb(0,216,255)', 'rgba(120,120,120,0.4)',],
    title: {
      show: false
    },
    tooltip: {},
    xAxis3D: {
      type: 'category',
      data: ['LoRa烟感', 'NB烟感'],
      axisLine: {
        lineStyle: {
          color: 'rgba(255,255,255,0.1)',
          width: 1
        },

      },
      splitLine: {
        show: false,
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        color: '#fff',

      }
    },
    yAxis3D: {
      show: false,
      type: 'category',
      data: [''],
      axisLine: {
        lineStyle: {
          color: 'rgba(0,0,0,0)',
          width: 1
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        color: '#fff',
        margin: 12
      }
    },
    zAxis3D: {
      type: 'value',
      axisLine: {
        lineStyle: {
          color: 'rgba(0,0,0,0)',
          width: 1
        }
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false,
      },
      axisLabel: {
        color: '#fff',
        margin: 12,
      }
    },
    grid3D: {
      boxWidth: 105,
      boxDepth: 40,
      axisPointer: {
        show: false
      },
      light: {
        main: {
          intensity: 1.4
        },
        ambient: {
          intensity: 0.1
        }
      },
      viewControl: {
        alpha: 1, //控制场景平移旋转
        beta: 0,
        minAlpha: 0,
        maxAlpha: 1,
        minBeta: 0,
        maxBeta: 0
      }
    },
    series: [{
      type: 'bar3D',
      name: '2',
      barSize: 12,
      data: [[0, 0, loraval], [1, 0, nbval]],
      stack: 'stack',
      shading: 'lambert',
      bevelSize: 0.1,
    },
      /*{
          type: 'bar3D',
          name: '3',
          barSize: 12,  
          data: [[0, 0, Math.max.apply(null, [loraval, nbval]) + 20 - loraval], [1, 0, Math.max.apply(null, [loraval, nbval]) + 20 - nbval]],
          stack: 'stack',
          shading: 'lambert',
          bevelSize: 0.1,
          itemStyle: {
              normal: {
                  label: {
                      show: false,//是否展示    
                  }
              }
  
          }
      }*/
    ]
  }
}