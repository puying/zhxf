
/**
 * 
 *
 */
 "use strict";
 import Vue from "vue";
 export default function treeToArray(
   data,
   parent = null,
   level = null
 ) {
   let tmp = [];
   Array.from(data).forEach(function(record) {
     let isChild = record.children ? record.children.length != 0 : false
     Vue.set(record, "expanded", isChild);
     let _level = 1;
     if (level !== undefined && level !== null) {
       _level = level + 1;
     }
     Vue.set(record, "level", _level);
     // 如果有父元素
     if (parent) {
       parent += ',' + record.parentId
       Vue.set(record, "parents", parent);
     } else {
       parent = record.parentId
     }
     tmp.push(record);
     if (record.children && record.children.length > 0) {
       const child = treeToArray(record.children, parent, _level);
       tmp = tmp.concat(child);
     }
   });
   return tmp;
 }