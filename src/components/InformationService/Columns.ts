export function getWireColumns(statusClick?: any): any {
  return [
    {
      prop: "fullName",
      label: "单位名称",
      headerAlign: "center",
      align: "left",
      width: "220px",
      isSort: true,
    },
    {
      prop: "addressCode",
      label: "设备ID",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "installAddress",
      label: "安装位置",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "createTime",
      label: "上报时间",
      headerAlign: "center",
      align: "left",
      width: "220px",
      isSort: true,
    },
    {
      prop: "warnStatus",
      label: "上报状态",
      headerAlign: "center",
      type: "tag",
      align: "center",
      onClick: statusClick,
    },
    {
      label: "详情",
      headerAlign: "center",
      align: "center",
      type: 'more'
    },
  ]
}
export function getElecColumns(statusClick?: any): any {
  return [
    {
      prop: "fullName",
      label: "单位名称",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "addressCode",
      label: "设备ID",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "installAddress",
      label: "安装位置",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "currentLeakage",
      label: "漏电值",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "current1",
      label: "电流值1（A）",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "current2",
      label: "电流值2（A）",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "current3",
      label: "电流值3（A）",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "temperature1",
      label: "温度1（℃）",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "temperature2",
      label: "温度2（℃）",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "temperature3",
      label: "温度3（℃）",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "temperature4",
      label: "温度4（℃）",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "createTime",
      label: "上报时间",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "warnStatus",
      label: "上报状态",
      headerAlign: "center",
      align: "center",
      type: "tag",

      onClick: statusClick,
    },
  ]
}

export function getDLogColumns(): any {
  return [
    {
      prop: "logType",
      label: "日志类型",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "fullName",
      label: "单位名称",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "installAddress",
      label: "安装位置",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "logSource",
      label: "日志名称",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "createTime",
      label: "记录时间",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
    {
      prop: "logText",
      label: "日志信息",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
  ]
}