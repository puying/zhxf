
export function setOptions(data: any) {
    return {
        color: ['rgb(0,216,255)', 'rgba(120,120,120,0.4)',],
        title: {
            show: false
        },
        tooltip: {},
        xAxis3D: {
            type: 'category',
            data: ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
            axisLine: {
                lineStyle: {
                    color: 'rgba(255,255,255,0.1)',
                    width: 1
                },

            },
            splitLine: {
                show: false,
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: '#fff',
                margin: 10,
                fontSize: 14,
            }
        },
        yAxis3D: {
            show: false,
            type: 'category',
            data: [],
            // max:dataMax,
            axisLine: {
                lineStyle: {
                    color: 'rgba(0,0,0,0)',
                    width: 1
                }
            },
            yxisLabel: {
                color: '#fff',
                fontSize: 14,
            },
            axisTick: {
                show: false
            }
        },
        zAxis3D: {
            type: 'value',
            max: 'dataMax',
            axisLine: {
                lineStyle: {
                    type: 'solid',
                    color: 'rgba(255,255,255,0.1)',
                    width: 1
                }
            },
            axisTick: {
                show: false
            },
            splitLine: {
                show: false,
            },
            axisLabel: {
                color: '#fff',
                margin: 12,
            }
        },
        grid3D: {
            boxWidth: 280,
            boxDepth: 40,
            axisPointer: {
                show: false
            },
            light: {
                main: {
                    intensity: 1.0
                },
                ambient: {
                    intensity: 0.1
                }
            },
            viewControl: {
                alpha: 0, //控制场景平移旋转
                beta: 0,
                minAlpha: 0,
                maxAlpha: 0,
                minBeta: 0,
                maxBeta: 0
            }
        },
        series: [{
            type: 'bar3D',
            name: '2',
            barSize: 10,
            bevelSize: 0.1,
            data: [[0, 0, data.Mon], [1, 0, data.Tue], [2, 0, data.Wed], [3, 0, data.Thu], [4, 0, data.Fri], [5, 0, data.Sat], [6, 0, data.Sun]],
            stack: 'stack',
            shading: 'lambert',
        },
        {
            type: 'bar3D',
            name: '3',
            barSize: 10,
            bevelSize: 0.1,
            data: [[0, 0, Math.max.apply(null, [data.Mon, data.Tue, data.Wed, data.Thu, data.Fri, data.Sat, data.Sun]) + 20 - data.Mon], [1, 0, Math.max.apply(null, [data.Mon, data.Tue, data.Wed, data.Thu, data.Fri, data.Sat, data.Sun]) + 20 - data.Tue], [2, 0, Math.max.apply(null, [data.Mon, data.Tue, data.Wed, data.Thu, data.Fri, data.Sat, data.Sun]) + 20 - data.Wed], [3, 0, Math.max.apply(null, [data.Mon, data.Tue, data.Wed, data.Thu, data.Fri, data.Sat, data.Sun]) + 20 - data.Thu], [4, 0, Math.max.apply(null, [data.Mon, data.Tue, data.Wed, data.Thu, data.Fri, data.Sat, data.Sun]) + 20 - data.Fri], [5, 0, Math.max.apply(null, [data.Mon, data.Tue, data.Wed, data.Thu, data.Fri, data.Sat, data.Sun]) + 20 - data.Sat], [6, 0, Math.max.apply(null, [data.Mon, data.Tue, data.Wed, data.Thu, data.Fri, data.Sat, data.Sun]) + 20 - data.Sun]],
            stack: 'stack',
            shading: 'lambert',
            itemStyle: {
                normal: {
                    label: {
                        show: false,//是否展示    
                    }
                }

            }
        }
        ]

    }
}
export default setOptions;