export const colors:any = {
  "success": {// options1
    color: "#1FDB45",
    colorList: [
      'rgb(70,228,89)', 'rgb(120,120,120)', 'rgba(0,0,0,0.1)', 'rgba(0,0,0,0.1)'
    ],
    borderColor1: 'rgb(70,228,89)',
    borderColor2: 'rgba(80,80,80,0.4)'
  },
  "danger": {// options2
    color: '#EB0F16',
    colorList: [ // 颜色组不同
      'rgb(255,16,24)', 'rgb(120,120,120)', 'rgba(0,0,0,0.1)', 'rgba(0,0,0,0.1)'
    ],
    borderColor1: 'rgba(255,16,24,1)',
    borderColor2: 'rgba(80,80,80,0.4)'
  },
  "warn": {// options3
    color: '#DB861F',
    colorList: [
      'rgb(240,147,34)', 'rgb(120,120,120)', 'rgba(10,77,94,1)', 'rgba(10,77,94,1)'
    ],
    borderColor1: 'rgb(240,147,34)',
    borderColor2: 'rgba(80,80,80,0.4)'
  },
  "info": {// options4
    color: '#99928F',
    colorList: [
      'rgb(156,156,156)', 'rgb(120,120,120)', 'rgba(0,0,0,0.1)', 'rgba(0,0,0,0.1)',
    ],
    borderColor1: 'rgb(156,156,156)',
    borderColor2: 'rgba(100,100,100,0.4)'
  }
}
export function setoptions (type:string, NormalCount:number, NotnormalCount:number) {
  let colorData = colors[type]
  return {
    backgroundColor: 'transparent',
    legend: {
        orient: 'vertical',
        icon: 'circle',
        x: '20',
        itemGap: 30,
        data: [],
        y: 'center',
    },
  
    // 提示框
    tooltip: {
        show: true,                 // 是否显示提示框
        formatter: '{c} ({d}%)',     // 提示框显示内容,此处{b}表示各数据项名称，此项配置为默认显示项，{c}表示数据项的值，默认不显示，({d}%)表示数据项项占比，默认不显示。
        backgroundColor: 'rgba(0,0,0,0.4)',
        textStyle: {
            color: colorData.color, // 颜色组
            decoration: 'none',
            fontFamily: 'Verdana, sans-serif',
            fontSize: 15,
            fontStyle: 'italic',
            fontWeight: 'bold'
        }
    },
    // 系列列表
    series: [{
        name: '',         // 系列名称
        type: 'pie',                    // 系列类型
        center: ['50%', '50%'],           // 饼图的中心（圆心）坐标，数组的第一项是横坐标，第二项是纵坐标。[ default: ['50%', '50%'] ]
        radius: ['0%', '60%'],         // 饼图的半径，数组的第一项是内半径，第二项是外半径。[ default: [0, '75%'] ]
        hoverAnimation: true,           // 圆环图的颜色
        itemStyle: {
            normal: {                       
                color: function (params:any) {
                    var colorList = colorData.colorList;
                    return colorList[params.dataIndex]
                },
                label: {
                    position: 'center',
                    textStyle: {
                        color: colorData.color// 这里要替换
                    }
                },
  
                labelLine: { show: false },
                show: false,
                
            }
        },
        data: [
            {
                value: NormalCount,
                color: colorData.color,
                highlight: colorData.color,                        
                name: "",                       
                 itemStyle: {
                      borderColor: colorData.borderColor1,
                      borderWidth: 12,
                  
                    },                    
                 emphasis:{
                    itemStyle:{
                        borderColor: colorData.borderColor1,
                        borderWidth: 12
                    }
                },
               /* label: {
                    show: true,
                    formatter: '正常{d}%',
                    position: 'center',                           
  
                }*/
            },
            {
                value: NotnormalCount,
                color: "rgba(0,0,0,0)",
                highlight: "rgba(0,0,0,0)",
                name: "",
                 itemStyle: {
                     borderColor: colorData.borderColor2,
                     borderWidth: 12,    
                },                            
            },
        ]                      
    }]
  }
}
export default setoptions;