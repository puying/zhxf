export default [
  {
    title: "本月报警数",
    icon: require("@/assets/images/month.png"),
    num: "1251",
    company: '次',
    theme: ''
  },
  {
    title: "设备报警率",
    icon: require("@/assets/images/round.png"),
    num: "5.33",
    company: '%',
    theme: ''
  },
  {
    title: "联网单位",
    icon: require("@/assets/images/house.png"),
    num: "497",
    company: '家',
    theme: ''
  },
  {
    title: "联网设备",
    icon: require("@/assets/images/charge.png"),
    num: "2033",
    company: '台',
    theme: ''
  },
  {
    title: "报警数",
    icon: require("@/assets/images/call_pce.png"),
    num: "50",
    company: '次',
    theme: 'danger'
  },
  {
    title: "故障数",
    icon: require("@/assets/images/warn.png"),
    num: "0",
    company: '次',
    theme: 'waring'
  },
  {
    title: "离线数",
    icon: require("@/assets/images/out_line.png"),
    num: "1479",
    company: '次',
    theme: 'info'
  },
  {
    title: "正常数",
    icon: require("@/assets/images/normal.png"),
    num: "503",
    company: '次',
    theme: 'success'
  }
];