import echarts from 'echarts'

export function setOptions(data: any) {
    return {
        backgroundColor: 'transparent',
        tooltip: {
            trigger: 'axis',
            formatter: function (params: any) {
                var tar = params[0];
                return tar.name + ': ' + tar.value;
            }
        },
        toolbox: {
            show: false
        },
        calculable: true,
        grid: {
            show: 'true',
            x: 40,
            y: 15,
            x2: 14,
            y2: 28,
            borderWidth: '0'
        },
        xAxis: [
            {
                type: 'category',
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: 'rgba(255,255,255,0.1)',
                        width: 1
                    }
                },

                splitLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(255,255,255,0.1)',
                        width: 1

                    }
                },
                boundaryGap: false,

                data: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"]
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLabel: {
                    formatter: '{value}',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: 'rgba(255,255,255,0.1)',
                        width: 1
                    }
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: 'rgba(255,255,255,0.1)',
                        width: 1

                    }
                },

                /* splitLine: { show: false }//去除网格线*/
            }
        ],
        series: [
            {
                type: 'line',
                smooth: true,
                itemStyle: {
                    normal:
                    {
                        areaStyle: {
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0, color: 'rgba(255,255,255,0.5)' // 0% 处的颜色
                            }, {
                                offset: 0.4, color: 'rgba(31,119,129,0.9)' // 100% 处的颜色
                            }, {
                                offset: 1, color: 'rgba(0,0,0,0.5)' // 100% 处的颜色
                            }]
                            ), //背景渐变色    
                        },
                        lineStyle: { color: 'rgb(94,214,215)' },
                        //color:'rgb(253,154,207)',
                    }
                },

                data: [data.Jan, data.Feb, data.Mar, data.Apr, data.May, data.Jun, data.Jul, data.Aug, data.Sept, data.Oct, data.Nov, data.Dec]
            }
        ]
    }
}
export default setOptions;