
export default function (datas: any) {
    let warn4 = Number(datas.normalCount);
    let warn3 = Number(datas.offlineCount);
    let warn2 = Number(datas.warnCount);
    let warn1 = Number(datas.breakdownCount);
    let options = {
        title: {
            show: false
        },
        tooltip: {
            formatter: function (params: any) {
                var tar = params[0];
                console.log(tar)
                return tar.name + '<br/>' + tar.seriesName + ' : ' + tar.value;
            }
        },
        toolbox: {
            show: true,
            right: '30',
            itemSize: 20,
            itemGap: 10,
            feature: {
                mark: { show: false, },
                restore: {
                    show: true,
                    iconStyle: {
                        color: 'rgba(255,255,255,0.5)'
                    },
                    emphasis: {
                        iconStyle: {
                            color: 'rgba(255,255,255,0.9)',
                            textAlign: 'right',
                        }
                    },
                },
                saveAsImage: {
                    show: true,
                    iconStyle: {
                        color: 'rgba(255,255,255,0.5)'
                    },
                    emphasis: {
                        iconStyle: {
                            color: 'rgba(255,255,255,0.9)',
                            textAlign: 'right',
                        }
                    },
                    backgroundColor: 'rgb(27,92,100)',
                }
            }
        },
        xAxis3D: {
            type: 'category',
            data: ["正常", "离线", "故障", "报警"],
            axisLine: {
                lineStyle: {
                    color: 'rgba(0,0,0,0)',
                    width: 1
                },
            },
            splitLine: {
                show: false,
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                color: '#fff',
                fontSize: 16
            }
        },
        yAxis3D: {
            show: false,
            type: 'category',
            data: [''],
            axisLine: {
                lineStyle: {
                    color: 'rgba(0,0,0,0)',
                    width: 1
                }
            },
            axisTick: {
                show: false
            }
        },
        zAxis3D: {
            type: 'value',

            axisLine: {
                lineStyle: {
                    color: 'rgba(0,0,0,0)',
                    width: 1
                }
            },
            axisTick: {
                show: false
            },
            splitLine: {
                show: false,
            },
            axisLabel: {
                color: '#fff'
            }
        },
        grid3D: {
            boxWidth: 240,
            boxDepth: 35,
            axisPointer: {
                show: false
            },
            light: {
                main: {
                    intensity: 1.4
                },
                ambient: {
                    intensity: 0.1
                }
            },
            viewControl: {
                alpha: 1, //控制场景平移旋转
                beta: 1,
                minAlpha: 0,
                maxAlpha: 1,
                minBeta: 0,
                maxBeta: 1
            }
        },
        series: [{
            type: 'bar3D',
            name: '2',
            barSize: 12,
            itemStyle: {
                normal: {
                    color: function (params: any) {
                        var colorList = ['rgb(29,197,62)', 'rgb(153,153,153)', 'rgb(243,152,1)', 'rgb(231,0,18)'];
                        return colorList[params.dataIndex];
                    },
                    label: {
                        show: true,

                        labelLine: { show: true },
                        textStyle: {
                            fontSize: 20
                        }
                    }
                }
            },
            bevelSize: 0.1,
            data: [[0, 0, warn4], [1, 0, warn3], [2, 0, warn2], [3, 0, warn1]],
            stack: 'stack',
            shading: 'lambert',
        },
        ]
    }
    return options
}