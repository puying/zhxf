
export default function (datas: any) {
    let warn4 = Number(datas.normalCount);
    let warn3 = Number(datas.offlineCount);
    let warn2 = Number(datas.warnCount);
    let warn1 = Number(datas.breakdownCount);
    let options = {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)",

        },
        toolbox: {
            show: true,
            right: '30',
            itemSize: 20,
            itemGap: 10,
            feature: {
                mark: { show: false, },
                restore: {
                    show: true,
                    iconStyle: {
                        color: 'rgba(255,255,255,0.5)',
                    },
                    emphasis: {
                        iconStyle: {
                            color: 'rgba(255,255,255,0.9)',
                            textAlign: 'right',
                        }
                    },
                },
                saveAsImage: {
                    show: true,
                    iconStyle: {
                        color: 'rgba(255,255,255,0.5)'
                    },
                    emphasis: {
                        iconStyle: {
                            color: 'rgba(255,255,255,0.9)',
                            textAlign: 'right',
                        }
                    },
                    backgroundColor: 'rgb(27,92,100)',
                },

            }
        },
        legend: {
            type: 'scroll',
            itemWidth: 15,
            orient: 'vertical',
            x: 'left',
            top: 20,
            itemGap: 10,
            data: [
                {
                    name: '正常', icon: 'stack',
                    textStyle: {
                        fontSize: 10,
                        fontWeight: '100',
                        color: '#fff',
                    },
                },
                {
                    name: '离线', icon: 'stack',
                    textStyle: {
                        fontSize: 10,
                        fontWeight: '100',
                        color: '#fff'
                    },
                },

                {
                    name: '报警', icon: 'stack',
                    textStyle: {
                        fontSize: 10,
                        fontWeight: '100',
                        color: '#fff'
                    },
                },
                {
                    name: '故障', icon: 'stack',
                    textStyle: {
                        fontSize: 10,
                        fontWeight: '100',
                        color: '#fff'
                    },
                }
            ],
            y: 'center',
        },
        calculable: true,
        series: [
            {
                name: '设备数量',
                type: 'pie',
                radius: ['40%', '60%'],
                center: ['50%', '50%'],
                roseType: 'radius',
                itemStyle: {
                    normal: {
                        color: function (params: any) {
                            var colorList = [
                                '#EC2614', '#99928F', '#F6BB08', '#50DB44'
                            ];
                            return colorList[params.dataIndex]
                        },
                        label: {
                            formatter: '{a|{d}%}\n {hr|{b} : {c}} ',
                            rich: {
                                a: {
                                    color: '#fff',
                                    fontSize: 23,
                                    lineHeight: 20,
                                    align: 'center'
                                },
                                hr: {
                                    color: 'rgba(255,255,255,0.6)',
                                    width: '100%',
                                    fontSize: 13,
                                    height: 0,
                                    alien: 'center'
                                }
                            }
                        },
                        labelLine: {
                            show: true,
                            lineStyle: { color: 'rgba(255,255,255,0.6)' }
                        },
                    }
                },
                data: [
                    { value: warn1, name: '报警' },

                    { value: warn3, name: '离线' },
                    { value: warn2, name: '故障' },
                    { value: warn4, name: '正常' }
                ]
            },
            {
                name: '设备数量',
                type: 'pie',
                radius: ['37%', '40%'],
                center: ['50%', '50%'],
                itemStyle: {
                    normal: {
                        color: function (params: any) {
                            var colorList = [
                                'rgba(236,38,20,0.8)', 'rgba(153,146,143,0.8)', 'rgba(246,187,8,0.8)', 'rgba(80,219,68,0.8)'
                            ];
                            return colorList[params.dataIndex]
                        },
                        label: {
                            show: false,

                            labelLine: { show: false }
                        }
                    }
                },
                data: [
                    { value: warn1, name: '报警' },

                    { value: warn3, name: '离线' },
                    { value: warn2, name: '故障' },
                    { value: warn4, name: '正常' }
                ]
            }
        ]
    }
    return options;
}