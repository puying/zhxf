
export default function (datas: any) {
    let breakLs = datas.breakdownList || []
    let normalLs = datas.normalList || []
    let timeLs = datas.timeList || []
    let unlineLs = datas.offlineList || []
    let warnLs = datas.warnList || []
    let options = {
        borderWidth: 0,
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        toolbox: {
            show: true,
            itemSize: 20,
            right: 10,
            itemGap: 15,
            orient: 'horizontal',
            feature: {
                magicType: {
                    show: true, type: ['stack', 'tiled'],
                    emphasis: {
                        iconStyle: {
                            color: 'rgba(255,255,255,0.9)',
                            textAlign: 'right',
                        }
                    },
                },
                restore: {
                    show: true,
                    emphasis: {
                        iconStyle: {
                            color: 'rgba(255,255,255,0.9)',
                            textAlign: 'right',
                        }
                    },
                },
                saveAsImage: {
                    show: true,
                    backgroundColor: 'rgb(27,92,100)',
                    emphasis: {
                        iconStyle: {
                            color: 'rgba(255,255,255,0.9)',
                            textAlign: 'right',
                        }
                    },
                }
            },
            iconStyle: {
                normal: {
                    color: 'rgba(255,255,255,0.5)',//设置颜色
                }
            },

        },
        legend: {
            itemWidth: 24,             // 图例图形宽度
            itemHeight: 20,
            data: [
                {
                    name: '正常', icon: 'stack',
                    textStyle: {
                        fontSize: 12,
                        fontWeight: 'bolder',
                        color: '#fff',
                    },
                },
                {
                    name: '离线', icon: 'stack',
                    textStyle: {
                        fontSize: 12,
                        fontWeight: 'bolder',
                        color: '#fff'
                    },
                },

                {
                    name: '报警', icon: 'stack',
                    textStyle: {
                        fontSize: 12,
                        fontWeight: 'bolder',
                        color: '#fff'
                    },
                },
                {
                    name: '故障', icon: 'stack',
                    textStyle: {
                        fontSize: 12,
                        fontWeight: 'bolder',
                        color: '#fff'
                    },
                }

            ],
            textStyle: {//图例文字的样式
                color: '#fff',
                borderRadius: 0
            },
            itemGap: 30,
            x: 'center',
            y: 'bottom',
            padding: [0, 50, 0, 0]
        },
        calculable: true,
        grid: {
            show: true,
            borderWidth: 0,//这样所有的坐标轴都没有了
        },
        xAxis: {
            type: 'category',
            data: timeLs,
            axisLabel: {
                show: true,
                textStyle: {
                    color: '#fff'
                }
            },
            axisLine: {
                lineStyle: {
                    type: 'solid',
                    color: '#73ADE6',
                    width: 1,   //这里是坐标轴的宽度,这样显示x轴，而不显示x轴对面的轴
                },
                onZero: true
            },
            splitLine: { show: false },
        },
        yAxis: {
            type: 'value',
            axisLabel: {
                formatter: '{value}',
                textStyle: {
                    color: '#fff'
                }
            },
            axisLine: {
                lineStyle: {
                    type: 'solid',
                    color: '#73ADE6',
                    width: 1,   //这里是坐标轴的宽度,这样显示y轴，而不显示y轴对面的轴
                },
                onZero: true
            },
            splitLine: { show: false },
        },
        series: [
            {
                name: '正常',
                type: 'bar',
                stack: 'Statu',
                itemStyle: {
                    normal: {
                        barBorderColor: '#50DB44',
                        color: '#50DB44'
                    }
                },
                data: normalLs
            },
            {
                name: '离线',
                type: 'bar',
                stack: 'Statu',
                itemStyle: {
                    normal: {
                        barBorderColor: '#99928F',
                        color: '#99928F'
                    }
                },
                data: unlineLs
            },
            {
                name: '报警',
                type: 'bar',
                stack: 'Statu',
                itemStyle: {
                    normal: {
                        barBorderColor: '#EC2614',
                        color: '#EC2614'
                    }
                },
                data: warnLs
            },
            {
                name: '故障',
                type: 'bar',
                stack: 'Statu',
                itemStyle: {
                    normal: {
                        barBorderColor: '#F6BB08',
                        color: '#F6BB08'
                    }
                },
                barMaxWidth: 40,//最大宽度
                data: breakLs
            }
        ]

    }

    return options
}