export function getLogColumn() {
  return [
    {
      prop: "userName",
      label: "用户名",
      headerAlign: "center",
      align: "left",
      width: "200px",
    },
    {
      prop: "operateType",
      label: "操作类型",
      headerAlign: "center",
      align: "left",
      width: "200px",
    },
    {
      prop: "ip",
      label: "ip",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "ipAddress",
      label: "ip所在地",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "result",
      label: "请求结果",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "createTime",
      label: "操作时间",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "remarks",
      label: "描述",
      headerAlign: "center",
      align: "left",
      width: "280px",
    },
  ]
}