// menu
export default [
  {
    text: "数据监控",
    icon: "fa-book",
    isView: false,
    name: "dataMonitoring",
    childs: [
      {
        text: "数据监控",
        icon: "fa-newspaper-o",
        name: "dataMonitoringChild",
        isView: true,
      },
      {
        text: "地图监控",
        name: "mapMonitoring",
        icon: "fa-globe",
        isView: true,
      }
    ]
  },
  {
    text: "统计分析",
    isView: false,
    name: "statisticalAnalysis",
    icon: "fa-balance-scale",
    childs: [
      {
        text: "实时报警统计",
        name: "realTimeAlarmStatistics",
        icon: "fa-bar-chart",
        isView: true,
      },
      {
        text: "时段报警统计",
        name: "timeIntervalAlarmStatistics",
        icon: "fa-camera-retro",
        isView: true,
      },
      {
        text: "统计报表",
        name: "statisticalReport",
        icon: "fa-file-text",
        isView: true,
      }
    ]
  },
  {
    text: "大屏显示",
    name: "largeScreenDisplay",
    icon: "fa-tv",
    isView: true,
  },
  {
    text: "信息查询",
    name: "informationService",
    isView: false,
    icon: "fa-search-plus",
    childs: [
      {
        text: "设备历史数据",
        name: "deviceHistoryData",
        icon: "fa-list-alt",
        isView: true,
      },
      {
        text: "设备通讯日志",
        name: "deviceCommunicationLog",
        icon: "fa-commenting",
        isView: true,
      }
    ]
  },
  {
    text: "消防信息",
    isView: false,
    name: "fireInformation",
    icon: "fa-newspaper-o",
    childs: [
      {
        text: "消防知识管理",
        name: "fireKnowledgeManagement",
        icon: "fa-info",
        isView: true,
      },
      {
        text: "公共信息管理",
        name: "publicInformationManagement",
        icon: "fa-info-circle",
        isView: true,
      }
    ]
  },
  {
    text: "信息管理",
    isView: false,
    name: "informationManagement",
    icon: "fa-database",
    childs: [
      {
        text: "设备信息管理",
        name: "equipmentInformationManagement",
        icon: "fa-fire",
        isView: true,
      },
      {
        text: "烟感远程配置",
        name: "remoteConfigurationOfSmokeDetector",
        icon: "fa-cog",
        isView: true,
      },
      {
        text: "用电远程配置",
        name: "remoteConfigurationOfPowerConsumption",
        icon: "fa-gear",
        isView: true,
      }
    ]
  },
  {
    text: "权限管理",
    name: "authorityManagement",
    isView: false,
    icon: "fa-coffee",
    childs: [
      {
        text: "联网单位",
        name: "networkingUnit",
        icon: "fa-recycle",
        isView: true,
      },
      {
        text: "角色管理",
        name: "roleManagement",
        icon: "fa-users",
        isView: true,
      },
      {
        text: "用户管理",
        name: "userManagement",
        icon: "fa-user",
        isView: true,
      }
    ]
  },
  {
    text: "系统管理",
    name: "systemManagement",
    isView: false,
    icon: "fa-cog",
    childs: [
      {
        text: "通用字典",
        name: "generalDictionary",
        icon: "fa-book",
        isView: true,
      },
      {
        text: "系统日志",
        name: "systemLog",
        icon: "fa-warning",
        isView: true,
      },
      {
        text: "系统功能",
        name: "systemFunction",
        icon: "fa-navicon",
        isView: true,
      }
    ]
  }
]