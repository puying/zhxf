export default {
  color: ['rgb(254,0,253)'],
  backgroundColor: 'transparent',
  tooltip: {
    trigger: 'axis'
  },
  toolbox: {
    show: false
  },
  calculable: true,
  grid: {
    show: 'true',
    x: 47,
    y: 45,
    x2: 14,
    y2: 28,
    borderWidth: '0'
  },
  legend: {
    data: ['温度',],
    textStyle: {
      fontSize: 12,
      color: 'rgba(255,255,255,0.8)'
    },
  },
  xAxis:
  {
    data: [],
    type: 'category',
    axisLabel: {
      show: true,
      textStyle: {
        color: 'rgba(255,255,255,0.6)'
      }
    },
    axisLine: {
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1
      }
    },
    splitLine: {
      show: false,
    },
    boundaryGap: false,

  },
  yAxis:
  {
    type: 'value',
    axisLabel: {
      formatter: '{value} ℃',
      textStyle: {
        color: 'rgba(255,255,255,0.5)'
      }
    },
    axisLine: {
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1
      }
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1

      }
    },

    /* splitLine: { show: false }//去除网格线*/
  },
  series: [
    {
      name: '温度',
      type: 'line',
      smooth: true,
      symbolSize: 0, //设置各个拐点的大小
      /* markPoint: {
           data: [
               { type: 'max', name: '最大值' },
               { type: 'min', name: '最小值' }
           ]
       },*/
      lineStyle: {
        color: 'rgb(254,0,253)'
      },
      data: []
    },
  ]
}