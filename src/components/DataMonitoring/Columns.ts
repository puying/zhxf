export function getWireColumns(statusClick?: any, exClick?: any): any {
  return [
    {
      prop: "fullName",
      label: "单位名称",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "meterType",
      label: "设备型号",
      headerAlign: "center",
      align: "left",
      width: "120px",
    },
    {
      prop: "addressCode",
      label: "设备ID",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "installAddress",
      label: "安装位置",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "updateTime",
      label: "更新时间",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "warnStatus",
      label: "产品状态",
      headerAlign: "center",
      align: "center",
      type: "tag",
      onClick: statusClick,
    }, {
      label: "异常指派",
      headerAlign: "center",
      align: "center",
      type: 'ExceptionAssignment',
      onClick: exClick,
      width: '90px'
    },
    {
      label: "详细信息",
      headerAlign: "center",
      align: "center",
      type: 'more',
    },
  ]
}
export function getElecColumns(statusClick?: any, exClick?: any): any {
  return [
    {
      prop: "fullName",
      label: "单位名称",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "meterType",
      label: "设备型号",
      headerAlign: "center",
      align: "left",
      width: "120px",
    },
    {
      prop: "addressCode",
      label: "设备ID",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "installAddress",
      label: "安装位置",
      headerAlign: "center",
      align: "left",
      width: "180px",
    }, {
      type: 'ExceptionAssignment',
      label: "异常指派",
      headerAlign: "center",
      align: "center",
      onClick: exClick,
    },
    {
      prop: "maxl",
      label: "最大漏电值（mA）",
      headerAlign: "center",
      align: "right",
    },
    {
      prop: "maxc",
      label: "最大电流值（A）",
      headerAlign: "center",
      align: "right",
      type: "maxc",
    },
    {
      prop: "maxt",
      label: "最高温度",
      headerAlign: "center",
      align: "right",
      type: "maxt",
    },
    {
      prop: "updateTime",
      label: "更新时间",
      headerAlign: "center",
      align: "left",
      width: "180px",
    },
    {
      prop: "warnStatus",
      label: "产品状态",
      headerAlign: "center",
      align: "center",
      type: "tag",
      onClick: statusClick,
    },
    {
      prop: "remarks",
      label: "详细信息",
      headerAlign: "center",
      align: "left",
      width: "220px",
    },
  ]
}