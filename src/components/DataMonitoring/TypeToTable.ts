export default [
  {
    type: "漏电回路1",
    value: "{v}/{w}mA",
    healthyVal: "100",
    celColor: 'rgb(236,104,64)'
  },
  {
    type: "电流回路1",
    value: "{v}/{w}A",
    healthyVal: "100",
    celColor: 'rgb(236,104,64)'
  }, {
    type: "电流回路2",
    value: "{v}/{w}A",
    healthyVal: "100",
    celColor: 'rgb(49,153,293)'
  }, {
    type: "电流回路3",
    value: "{v}/{w}A",
    healthyVal: "100",
    celColor: 'rgb(254,0,254)'
  }, {
    type: "温度回路1",
    value: "{v}/{w}℃",
    healthyVal: "100",
    celColor: 'rgb(236,104,64)'
  }, {
    type: "温度回路2",
    value: "{v}/{w}℃",
    healthyVal: "100",
    celColor: 'rgb(49,153,293)'
  }, {
    type: "温度回路3",
    value: "{v}/{w}℃",
    healthyVal: "100",
    celColor: 'rgb(254,0,254)'
  }, {
    type: "温度回路4",
    value: "{v}/{w}℃",
    healthyVal: "100",
    celColor: 'rgb(37,234,69)'
  }
]