export default {
  title: {
    text: '',
    subtext: ''
  },
  grid: {
    show: true,
    x: 45,
    y: 45,
    x2: 14,
    y2: 38,
    borderWidth: 0,//这样所有的坐标轴都没有了
  },
  tooltip: {
    trigger: 'axis'
  },
  toolbox: {
    show: false
  },
  legend: {
    data: ['烟感电压', '底座电压'],
    textStyle: {
      fontSize: 12,
      color: 'rgba(255,255,255,0.8)'
    },
  },
  calculable: true,
  xAxis: {
    type: 'category',
    data: [],
    axisLabel: {
      interval: 18,
      show: true,
      textStyle: {
        color: 'rgba(255,255,255,0.6)'
      }
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1

      }
    },
    axisLine: {
      lineStyle: {
        type: 'solid',
        color: 'rgba(255,255,255,0.2)',
        width: 1,   //这里是坐标轴的宽度,这样显示x轴，而不显示x轴对面的轴
      },
      onZero: true
    },
  },
  yAxis: {
    type: 'value',
    axisLabel: {
      formatter: '{value}',
      textStyle: {
        color: 'rgba(255,255,255,0.5)'
      }
    },
    splitLine: { show: false },
    axisLine: {
      lineStyle: {
        type: 'solid',
        color: 'rgba(255,255,255,0.2)',
        width: 1,   //这里是坐标轴的宽度,这样显示y轴，而不显示y轴对面的轴
      },
      onZero: true
    },

  },
  series: [
    {
      name: '烟感电压',
      type: 'line',
      smooth: true,
      symbol: 'circle', //设置拐点格式样式 如:实心圆，空心圆或不显示拐点等
      itemStyle: {
        normal: {
          lineStyle: {
            color: "rgb(236,104,65)", //设置拐点颜色
            width: 2 //设置各个拐点连接的线条宽度
          },
        }
      },
      symbolSize: 1, //设置各个拐点的大小
      //markPoint: {                    
      //    data: [
      //        { type: 'max', name: '最大值' },
      //        { type: 'min', name: '最小值' }
      //    ]
      //},
      data: []
    },
    {
      name: '底座电压',
      type: 'line',
      smooth: true,
      symbol: 'circle', //设置拐点格式样式 如:实心圆，空心圆或不显示拐点等
      itemStyle: {
        normal: {
          lineStyle: {
            color: "rgb(1,228,228)", //设置拐点颜色
            width: 2 //设置各个拐点连接的线条宽度
          },
        }
      },
      symbolSize: 1, //设置各个拐点的大小
      //markPoint: {
      //    data: [
      //        { type: 'max', name: '最大值' },
      //        { type: 'min', name: '最小值' }
      //    ]
      //},
      data: []
    }
  ]
}