// 漏电值
let optionL = {
  title: {
    text: '',
    subtext: ''
  },
  grid: {
    x: 55,
    y: 50,
    x2: 20,
    y2: 25,
    borderWidth: 0
  },
  tooltip: {
    trigger: 'axis',

  },
  toolbox: {
    show: false
  },
  legend: {
    data: ['漏电值'],
    textStyle: {
      color: 'rgba(255,255,255,0.8)'
    }
  },
  // calculable: true,
  xAxis: {
    type: 'category',
    data: [],
    axisLabel: {
      show: true,
      textStyle: {
        color: 'rgba(255,255,255,0.8)'
      }
    },
    axisLine: {
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1
      }
    },
    splitLine: {
      show: false
    }

  },
  yAxis: {
    type: 'value',
    axisLabel: {
      formatter: '{value} mA',
      textStyle: {
        color: 'rgba(255,255,255,0.5)'
      }
    },
    axisLine: {
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1
      }
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1

      }
    },

  },
  series: [
    {
      name: '漏电值',
      type: 'line',
      data: [],
      smooth: true,
      symbol: 'circle',
      showSymbol: false,
      symbolSize: 0,
      lineStyle: {

      }

    }
  ]
}
// 电流
let optionC = {
  title: {
    text: '',
    subtext: ''
  },
  grid: {
    x: 55,
    y: 50,
    x2: 20,
    y2: 25,
    borderWidth: 0
  },
  tooltip: {
    trigger: 'axis',

  },
  toolbox: {
    show: false
  },
  legend: {
    data: ['电流值1', '电流值2', '电流值3'],
    textStyle: {
      color: 'rgba(255,255,255,0.8)'
    }
  },
  // calculable: true,
  xAxis: {
    type: 'category',
    data: [],
    axisLabel: {
      show: true,
      textStyle: {
        color: 'rgba(255,255,255,0.8)'
      }
    },
    axisLine: {
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1
      }
    },
    splitLine: {
      show: false
    }
  },
  yAxis: {
    type: 'value',
    axisLabel: {
      formatter: '{value} A',
      textStyle: {
        color: 'rgba(255,255,255,0.5)'
      }
    },
    axisLine: {
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1
      }
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1

      }
    },
  },
  series: [
    {
      name: '电流值1',
      type: 'line',
      smooth: true,
      // markPoint: {
      //     data: [
      //         { type: 'max', name: '最大值' },
      //         { type: 'min', name: '最小值' }
      //     ]
      // },
      data: [],
      symbol: 'circle',
      showSymbol: false,
      symbolSize: 0,
      lineStyle: {
        width: 5
      }
    },
    {
      name: '电流值2',
      type: 'line',
      smooth: true,
      // markPoint: {
      //     data: [
      //         { type: 'max', name: '最大值' },
      //         { type: 'min', name: '最小值' }
      //     ]
      // },
      data: [],
      symbol: 'circle',
      showSymbol: false,
      symbolSize: 0,
    },
    {
      name: '电流值3',
      type: 'line',
      smooth: true,
      // markPoint: {
      //     data: [
      //         { type: 'max', name: '最大值' },
      //         { type: 'min', name: '最小值' }
      //     ]
      // },
      data: [],
      symbol: 'circle',
      showSymbol: false,
      symbolSize: 0,
    }
  ]
}
// 温度
let optionT = {
  title: {
    text: '',
    subtext: ''
  },
  grid: {
    x: 44,
    y: 50,
    x2: 20,
    y2: 25,
    borderWidth: 0
  },
  tooltip: {
    trigger: 'axis',

  },
  toolbox: {
    show: false
  },
  legend: {
    data: ['温度1', '温度2', '温度3', '温度4'],
    textStyle: {
      color: 'rgba(255,255,255,0.8)'
    }
  },
  // calculable: true,
  xAxis: {
    type: 'category',
    data: [],
    axisLabel: {
      show: true,
      textStyle: {
        color: 'rgba(255,255,255,0.8)'
      }
    },
    axisLine: {
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1
      }
    },
    splitLine: {
      show: false
    }
  },
  yAxis: {
    type: 'value',
    axisLabel: {
      formatter: '{value} ℃',
      textStyle: {
        color: 'rgba(255,255,255,0.5)'
      }
    },
    axisLine: {
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1
      }
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: 'rgba(255,255,255,0.1)',
        width: 1

      }
    },
  },
  series: [
    {
      name: '温度1',
      type: 'line',
      smooth: true,
      // markPoint: {
      //     data: [
      //         { type: 'max', name: '最大值' },
      //         { type: 'min', name: '最小值' }
      //     ]
      // },
      data: [],
      symbol: 'circle',
      showSymbol: false,
      symbolSize: 0,
    },
    {
      name: '温度2',
      type: 'line',
      smooth: true,
      // markPoint: {
      //     data: [
      //         { type: 'max', name: '最大值' },
      //         { type: 'min', name: '最小值' }
      //     ]
      // },
      data: [],
      symbol: 'circle',
      showSymbol: false,
      symbolSize: 0,
    },
    {
      name: '温度3',
      type: 'line',
      smooth: true,
      // markPoint: {
      //     data: [
      //         { type: 'max', name: '最大值' },
      //         { type: 'min', name: '最小值' }
      //     ]
      // },
      data: [],
      symbol: 'circle',
      showSymbol: false,
      symbolSize: 0,
    },
    {
      name: '温度4',
      type: 'line',
      smooth: true,
      // markPoint: {
      //     data: [
      //         { type: 'max', name: '最大值' },
      //         { type: 'min', name: '最小值' }
      //     ]
      // },
      data: [],
      symbol: 'circle',
      showSymbol: false,
      symbolSize: 0,
    }
  ]
}
export {
  optionL,
  optionC,
  optionT
}