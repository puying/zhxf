import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

// urls和https挂载
import service from "./utils/https";
import urls from "./utils/urls";

Vue.prototype.$https = service; // 其他页面在使用 axios 的时候直接  this.$http 就可以了
Vue.prototype.$urls = urls; // 其他页面在使用 urls 的时候直接  this.$urls 就可以了

import 'element-ui/lib/theme-chalk/index.css'
// 图标字体文件
import 'font-awesome/css/font-awesome.min.css'

import { Message, MessageBox, Loading, Notification } from "element-ui";

Vue.use(Loading.directive)
Vue.prototype.$loading = Loading.service
Vue.prototype.$message = Message;
Vue.prototype.$notify = Notification;
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$msgbox = MessageBox

Vue.config.errorHandler = (err, vm, info) => {
  let { message, name, stack } = err
  // 在vue提供的error对象中，script、line、column目前是空的。但这些信息其实在错误栈信息里可以看到。
  // script = !_.isUndefined(script) ? script : '';
  // line = !_.isUndefined(line) ? line : 0;
  // column = !_.isUndefined(column) ? line : 0;
  // 解析错误栈信息
  if (/Loading [CSS ]{0,1}chunk (.*) failed\./.test(message)) {
    console.group('分组')
    console.info('catch Loading chunk booking-order-goods-info failed.')
    let stackStr = stack ? stack.toString() : `${name}:${message}`

    console.info(stackStr)
    console.info(message)
  }
}

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
