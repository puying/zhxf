import Vue from 'vue'
import Component from 'vue-class-component'
import qs from 'qs'
let UserObj: any = {};
@Component
export default class commMixins extends Vue {
  public loadingInstance: any;
  // 公共方法类
  goBack() {
    this.$router.go(-1)
  }
  commTest() {
    console.log('公共方法测试')
  }
  setUserInfo(info: any) {
    console.log(info)
    UserObj = info
  }
  getUserInfo() {
    console.log(this)
    return UserObj
  }
  getMaxc(row: any) {
    let cellvalue = row.maxc
    if (cellvalue == null)
      return "";

    let valStr = "";
    if (cellvalue == row.c1)
      valStr += "/A";
    if (cellvalue == row.c2)
      valStr += "/B";
    if (cellvalue == row.c3)
      valStr += "/C";
    return cellvalue + " " + valStr.substr(1) + "相";
  }
  getMaxt(rowObject: any) {
    let cellvalue = rowObject.maxt
    if (cellvalue == null)
      return "";

    var valStr = cellvalue + " ";
    if (cellvalue == rowObject.t1)
      valStr += "/T1";
    if (cellvalue == rowObject.t2)
      valStr += "/T2";
    if (cellvalue == rowObject.t3)
      valStr += "/T3";
    if (cellvalue == rowObject.t4)
      valStr += "/T4";
    if (cellvalue == rowObject.t5)
      valStr += "/T5";
    if (cellvalue == rowObject.t6)
      valStr += "/T6";
    if (cellvalue == rowObject.t7)
      valStr += "/T7";
    if (cellvalue == rowObject.t8)
      valStr += "/T8";
    return valStr;
  }
  alertMsg(d: any) {
    let self = this
    // if (d.code > 10000) {
    //   self.$message.error(d.message);
    // } else if (d.code < 10000) {
    //   self.$message.error(d.data);
    // }
    if (d.code > 10000) {
      self.$notify.error({
        title: '错误',
        message: d.message
      })
    } else if (d.code < 10000) {
      self.$notify.error({
        title: '错误',
        message: d.data
      })
    }
  }
  getUrl(key: string, req: any, hisReq: any): string {
    let self = this
    let url = self.$urls[key]
    if (hisReq) {
      for (let key in hisReq) {
        url = url.replace('{' + key + '}', hisReq[key])
      }
    }
    let query = qs.stringify(req)
    url += '?' + query
    let myUrl = 'http://iot.kiddecloud.com' + url
    return myUrl
  }
  post(key: string, req: any, hisReq: any) {
    let self = this
    return new Promise((resolve, reject) => {
      let url = self.$urls[key]
      if (!url) {
        self.$message.error('没有这个api');
        return
      }
      if (hisReq) {
        for (let key in hisReq) {
          url = url.replace('{' + key + '}', hisReq[key])
        }
      }

      self.$https.post(url, req)
        .then((res: any) => {
          let d = res.data
          self.alertMsg(d)
          resolve(d)
        })
        .catch((res: any) => {
          reject(res.data)
        })
    })

  }
  get(key: string, params: any, hisReq: any) {
    let self = this
    return new Promise((resolve, reject) => {
      let url = self.$urls[key]
      if (!url) {
        self.$message.error('没有这个api');
        return
      }
      if (hisReq) {
        for (let key in hisReq) {
          url = url.replace('{' + key + '}', hisReq[key])
        }
      }

      let req = {
        params: params
      }
      self.$https.get(url, req)
        .then((res: any) => {
          let d = res.data
          self.alertMsg(d)
          resolve(d)
        })
        .catch((res: any) => {
          reject(res.data)
        })
    })
  }
  download(key: string, params: any, hisReq: any) {
    let self = this
    return new Promise((resolve, reject) => {
      let url = self.$urls[key]
      if (!url) {
        self.$message.error('没有这个api');
        return
      }
      if (hisReq) {
        for (let key in hisReq) {
          url = url.replace('{' + key + '}', hisReq[key])
        }
      }
      let timeout = new Date().getTime
      let config = {
        headers: {
          'Content-Type': 'application/octet-stream;charset=UTF-8',
          'Content-Disposition': 'attachment;filename=excel_' + timeout,
          'Pargam': 'no-cache',
          'Cache-Control': 'no-cache'
        },

        responseType: 'blob',
        params: params
      }
      self.$https.get(url, config)
        .then((res: any) => {
          let d = res
          resolve(d)
        })
        .catch((res: any) => {
          reject(res.data)
        })
    })
  }
  put(key: string, params: any, hisReq: any) {
    let self = this
    return new Promise((resolve, reject) => {
      let url = self.$urls[key]
      if (!url) {
        self.$message.error('没有这个api');
        return
      }
      if (hisReq) {
        for (let key in hisReq) {
          url = url.replace('{' + key + '}', hisReq[key])
        }
      }
      self.$https.put(url, params)
        .then((res: any) => {
          let d = res.data
          self.alertMsg(d)
          resolve(d)
        })
        .catch((res: any) => {
          reject(res.data)
        })
    })

  }
  formatDate(oldDate: any) {
    var date = new Date(oldDate);
    var YY = date.getFullYear() + '-';
    var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
    var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return YY + MM + DD;
  }
  delete(key: string, params: any, hisReq: any) {
    let self = this
    return new Promise((resolve, reject) => {
      let url = self.$urls[key]
      if (!url) {
        self.$message.error('没有这个api');
        return
      }
      if (hisReq) {
        for (let key in hisReq) {
          url = url.replace('{' + key + '}', hisReq[key])
        }
      }
      self.$https.delete(url, params)
        .then((res: any) => {
          let d = res.data
          self.alertMsg(d)
          resolve(d)
        })
        .catch((res: any) => {
          reject(res.data)
        })
    })

  }

}