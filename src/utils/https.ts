import axios from "axios";
import { Message } from "element-ui";
import { getStorage } from './utils'
import qs from 'qs'

// 创建axios实例
let service: any = {};
let href = window.location.host

let baseUrl = '/'
if (href.indexOf('localhost') > -1) {
  baseUrl = '/utils'
} else {
  baseUrl = '/'
}

service = axios.create({
  baseURL: baseUrl, // api的base_url
  timeout: 50000, // 请求超时时间
  headers: {
    "Content-Type": "application/json;charset=UTF-8"
  },
  transformRequest: [function (data) {

    return JSON.stringify(data)
  }]
});

// request拦截器 axios的一些配置
service.interceptors.request.use(
  (config: any) => {
    config.headers.common['XAToken'] = 'Bearer ' + getStorage('XAToken')
    return config;
  },
  (error: any) => {
    // Do something with request error
    console.error("error:", error); // for debug
    Promise.reject(error);
  }
);

// respone拦截器 axios的一些配置
service.interceptors.response.use(
  (response: any) => {
    return response;
  },
  (error: any) => {
    console.error("error:", error); // for debug
    if (error.response.status == 401) {
      Message.error('登录超时！')
      setTimeout(() => {
        window.location.href = "/#/Login"
      }, 1000)
    }
    return Promise.reject(error);
  }
);

export default service;