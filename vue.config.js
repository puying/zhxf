	const webpack = require('webpack')
const TerserPlugin = require('terser-webpack-plugin')
const path = require("path");
const sourceMap = process.env.NODE_ENV === "development";

const CompressionWebpackPlugin = require('compression-webpack-plugin');
// 定义压缩文件类型
const productionGzipExtensions = /\.(js|css|json|txt|html|ico|svg)(\?.*)?$/i

module.exports = {
  // 基本路径
  publicPath: "./",
  // 输出文件目录
  outputDir: "dist",
  // eslint-loader 是否在保存的时候检查
  lintOnSave: false,
  // webpack配置
  // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  chainWebpack: (config) => {
    config.plugin('preload').tap(() => [{
      rel: 'preload',
      fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
      include: 'initial'
    }])
    config.plugins.delete('prefetch')
    config.when(process.env.NODE_ENV !== 'development', config => {
      config.optimization.splitChunks({
        chunks: 'all',
        cacheGroups: {
          libs: {
            name: 'chunk-libs',
            test: /[\\/]node_modules[\\/]/,
            priority: 10,
            chunks: 'initial' // only package third parties that are initially dependent
          },
          elementUI: {
            name: 'chunk-elementUI', // split elementUI into a single package
            priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
            test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
          }
        }
      })
      config.optimization.runtimeChunk('single')
    })
    if (process.env.NODE_ENV === 'production') {
    //   config.plugin()
    //     .use(new CompressionWebpackPlugin({
    //         filename: '[path].gz[query]',
    //         algorithm: 'gzip',
    //         test: productionGzipExtensions,//匹配文件名
    //         threshold: 10240,//对10K以上的数据进行压缩
    //         minRatio: 0.8,
    //         deleteOriginalAssets:true,//是否删除源文件
    //       }))
    }
  },
  configureWebpack: config => {
    if (process.env.NODE_ENV === "production") {
      // 为生产环境修改配置...
      config.mode = "production";
      
      config.optimization = {
        runtimeChunk: 'single',
        splitChunks: {
          chunks: 'all',
          maxInitialRequests: Infinity,
          minSize: 20000,
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
                return `npm.${packageName.replace('@', '')}`
              }
            }
          }
        },
        minimizer: [
          new TerserPlugin({
            terserOptions: {
              warnings: false,
                compress: { 
                  drop_console: true,
                  drop_debugger: true,
                  pure_funcs: ['console.log']
                }
            }
          })
        ]
      }
      config.performance = {
        hints: 'warning',
        maxEntrypointSize: 50000000,
        assetFilter: function (assetFilename) {
          return assetFilename.endsWith('.js');
        }
      }
    } else {
      // 为开发环境修改配置...
      config.mode = "development";
    }

    Object.assign(config, {
      // 开发生产共同配置
      resolve: {
        extensions: [".js", ".vue", ".json", ".ts", ".tsx"],
        alias: {
          vue$: "vue/dist/vue.js",
          "@": path.resolve(__dirname, "./src")
        }
      }
    });

  },
  // 生产环境是否生成 sourceMap 文件
  productionSourceMap: sourceMap,
  // css相关配置
  css: {
    // 是否使用css分离插件 ExtractTextPlugin
    extract: false,
    // 开启 CSS source maps?
    sourceMap: false,
    // css预设器配置项
    loaderOptions: {},
    // 启用 CSS modules for all css / pre-processor files.
    modules: false
  },
  // use thread-loader for babel & TS in production build
  // enabled by default if the machine has more than 1 cores
  parallel: require("os").cpus().length > 1,
  // PWA 插件相关配置
  // see https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
  pwa: {},
  // webpack-dev-server 相关配置
  devServer: {
    open: false,
    host: "localhost",
    port: 8080, //8080,
    https: false,
    hotOnly: false,
    proxy: {
      // 设置代理
      // proxy all requests starting with /api to jsonplaceholder
      "/utils": {
        // target: "https://emm.cmccbigdata.com:8443/",
        target: 'http://iot.kiddecloud.com/',
        changeOrigin: true,
        secure: false,
        logLevel: 'debug',
        ws: true,
        headers: {
          host: 'iot.kiddecloud.com',
          origin: 'http://iot.kiddecloud.com',
          referer: 'http://iot.kiddecloud.com/'
        },
        onProxyRes: (proxyRes, req, res) => {
          // 响应
          // console.log(proxyRes, req, res)
        },
        onProxyReq: (proxyReq, req, res) => {
          // 请求
          //console.log(proxyReq, req, res)
          proxyReq.setHeader('host', 'iot.kiddecloud.com')
          proxyReq.setHeader('origin', 'http://iot.kiddecloud.com')
          proxyReq.setHeader('referer', 'http://iot.kiddecloud.com/')
        },
        cookieDomainRewrite: {
          'iot.kiddecloud.com': '127.0.0.1'
        },
        pathRewrite: {
          "^/utils": ""
        }
      }
    },
    before: app => {}
  },
  // 第三方插件配置
  pluginOptions: {
    // ...
  }
};